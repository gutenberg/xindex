
function findSequences(a)
  local b = {}
  local firstPage = a[1]
  for i=2,#a do
    if a[i]-a[i-1] > 1 then
      lastPage = a[i-1]
      if lastPage - firstPage > 2 then
        b[#b+1]= tostring(firstPage).."--"..tostring(lastPage)
      else
        b[#b+1]= firstPage
        if (firstPage ~= lastPage) then b[#b+1]= lastPage end
      end 
      firstPage=a[i]
    end
  end
  if a[#a] - firstPage > 2 then  -- test file end
    b[#b+1]= tostring(firstPage).."--"..tostring(a[#a])
  else
    b[#b+1]= firstPage
    if (firstPage ~= a[#a]) then b[#b+1]= a[#a] end
  end 
  return (b)
end

local args = {...}
local file
if #args == 0 then 
  io.write ("Dateiname: ")
  file = io.read()
else
  file = args[1]
  print ("Dateiname: "..file)
end


table.sort(pages)
sortedPages = {}
for _,v in ipairs(pages) do
  sortedPages[#sortedPages+1] = tonumber(v)
end
for _,v in ipairs(sortedPages) do
  print(v)
end

sortedPages = findSequences(sortedPages)

print ("Ausgabe mit Seitenbereich")

for _,v in ipairs(sortedPages) do
  print(v)
end
