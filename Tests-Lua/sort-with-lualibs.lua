
dofile("/usr/local/texlive/current/texmf-dist/tex/luatex/lualibs/lualibs.lua")
--dofile("lualibs.lua")
require ("lualibs-string")
require ("lualibs-unicode")

print("Hello")

local n = 100000
local str = string.rep("123456àáâãäå",100)
    --
--[[
for i=-15,15,1 do
    for j=-15,15,1 do
        if utf.xsub(str,i,j) ~= utf.sub(str,i,j) then
            print("error",i,j,"l>"..utf.xsub(str,i,j),"s>"..utf.sub(str,i,j))
        end
    end
    if utf.xsub(str,i) ~= utf.sub(str,i) then
        print("error",i,"l>"..utf.xsub(str,i),"s>"..utf.sub(str,i))
    end
end

print(" 1, 7",utf.xsub(str, 1, 7),utf.sub(str, 1, 7))
print(" 0, 7",utf.xsub(str, 0, 7),utf.sub(str, 0, 7))
print(" 0, 9",utf.xsub(str, 0, 9),utf.sub(str, 0, 9))
print(" 4   ",utf.xsub(str, 4   ),utf.sub(str, 4   ))
print(" 0   ",utf.xsub(str, 0   ),utf.sub(str, 0   ))
print(" 0, 0",utf.xsub(str, 0, 0),utf.sub(str, 0, 0))
print(" 4, 4",utf.xsub(str, 4, 4),utf.sub(str, 4, 4))
print(" 4, 0",utf.xsub(str, 4, 0),utf.sub(str, 4, 0))
print("-3, 0",utf.xsub(str,-3, 0),utf.sub(str,-3, 0))
print(" 0,-3",utf.xsub(str, 0,-3),utf.sub(str, 0,-3))
print(" 5,-3",utf.xsub(str,-5,-3),utf.sub(str,-5,-3))
print("-3   ",utf.xsub(str,-3   ),utf.sub(str,-3   ))

]]

print (utf.tocodes("Ü",","))

print(" 1, 7",utf.sub(str, 1, 7))
print(" 0, 7",utf.sub(str, 0, 7))
