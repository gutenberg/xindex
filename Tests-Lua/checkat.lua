str = {"whatever",                 -- do nothing
       "whatever!whenever!time",   -- do nothing
--
-- the following should replace only "little" by "big"
-- means "little@big"  -> "big"
--
	"little@big",  
	"Size!little@big",
	"Whatever!Size!little@big!whatever",
	"Whatever!little@big!whatever!lile@big",
	"Whatever!little@big!whatever!littl@bg!litle@bi",
	"Whatever!little@b!whatever!ttle@big!lle@bi|foo",
	"Beamer-Template!navigation symbols@\\texttt {navigation symbols}"
}



for i=1,#str do print (str[i]:gsub('[^!|@%s]+@', '')) end


str = "Beamer-Template!navigation symbols@\\texttt {navigation symbols}"


print(str:gsub('[^!|@%s]+@', ''))local lpeg = assert(require"lpeg")
local C, S = lpeg.C, lpeg.S

local sep = S("@!|")
local str = C((1 - sep)^0)

local idx = str * ( "@" * str / function(match) return "@" .. match end
                  + "!" * str / function(match) return "!" .. match end
                  + "|" * str / function(match) return "|" .. match end)^0

print(idx:match("hello!world@foo|bar"))
print(idx:match("Beamer-Template!navigation symbols@\\texttt {navigation symbols"))
