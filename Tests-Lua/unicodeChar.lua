function Lower(c)
  local i
  local str=""
  for i = 1,#c-1 do 
    if c == "Ä" then str = str.."ä"
    elseif c == "Ö" then str = str.."ö"
    elseif c == "Ü" then str = str.."ü"
    else str = str..string.lower(c)
    end
  end
  return str
end

do
  local bytemarkers = { {0x7FF,192}, {0xFFFF,224}, {0x1FFFFF,240} }
  function utf8(decimal)
    if decimal<128 then return string.char(decimal) end
    local charbytes = {}
    for bytes,vals in ipairs(bytemarkers) do
      if decimal<=vals[1] then
        for b=bytes+1,2,-1 do
          local mod = decimal%64
          decimal = (decimal-mod)/64
          charbytes[b] = string.char(128+mod)
        end
        charbytes[1] = string.char(vals[2]+decimal)
        break
      end
    end
    return table.concat(charbytes)
  end
end

c=utf8(0x24)    print(c.." is "..#c.." bytes.") --> $ is 1 bytes.
for i=1,#c-1 do io.write(tostring(c[i]).." ") end print ()
c=utf8(0xA2)    print(c.." is "..#c.." bytes.") --> ¢ is 2 bytes.
c=utf8(0x20AC)  print(c.." is "..#c.." bytes.") --> € is 3 bytes.  
c=utf8(0x24B62) print(c.." is "..#c.." bytes.") --> 𤭢 is 4 bytes.   
c=utf8(0xDC) print(c.." is "..#c.." bytes.") --> 𤭢 is 4 bytes.   


text = "Überfall"
firstChar = string.sub("Überfall",1,1)

if firstChar:byte() == 195 then print("Ist ein Ü") else print("Ist kein Ü") end 

print (Lower("Ü").."Ü")

local Umlaute = "ÄÖÜäöü"
   if string.find(Umlaute,"x",1,true) ~= nil then print(2) else print(1) end
