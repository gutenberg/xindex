#!/bin/bash

#TL=`kpsexpand '$TEXMF'`
TL="/usr/local/texlive/texmf-dist"
if [ -d "$TL/scripts" ]
then 
  echo "$TL/scripts exists"
else
  mkdir -vp $TL/scripts
fi
if [ -d "$TL/tex/lualatex/xindex" ]
then 
  echo "$TL/tex/lualatex/xindex exists"
else
  mkdir -vp $TL/tex/lualatex/xindex
fi
#if [ -d "$TL/tex/lualatex/xindex/pl" ]
#then 
#  echo "$TL/tex/lualatex/xindex/pl exists"
#else
#  mkdir -vp $TL/tex/lualatex/xindex/pl
#fi
echo "copy files ..."

scp ./xindex.lua $TL/scripts/xindex/
scp *.lua $TL/tex/lualatex/xindex/

#if [ -d "/usr/local/share/lua/5.2" ]
#then
#  scp -r /usr/local/share/lua/5.2/pl $TL/tex/lualatex/xindex/
#else
#  scp -r /usr/local/share/lua/5.3/pl $TL/tex/lualatex/xindex/
#fi

TEXLUA=`which texlua`
BINDIR=`dirname $TEXLUA`
echo "BINDIR=$BINDIR"

if [ -e "$BINDIR/xindex" ] 
then 
  echo "Link exists"
else
  sudo ln -s $TL/scripts/xindex.lua /$BINDIR/xindex
fi
texhash
echo "... done"
