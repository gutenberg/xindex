#!/bin/sh

read version < "xindex.version"
versionOld=$version
if [ $versionOld -lt 100 ] ; 
  then
    versionOld="0.$versionOld"
  else
    versionOld=$((versionOld - 100))
    if [ $versionOld -lt 10 ] ;
    then
      versionOld="1.0$versionOld"
    else
      versionOld="1.$versionOld"    
    fi
fi
echo $versionOld
version=$((version + 1))
echo $version > "xindex.version"

if [ $version -lt 100 ] ; 
  then
    version="0.$version"
  else
    version=$((version - 100))
    if [ $version -lt 10 ] ;
    then
      version="1.0$version"
    else
      version="1.$version"    
    fi
fi

echo "Update version number to $version ..."
perl -pi -e "~s/version = $versionOld/version = $version/g" *.lua
perl -pi -e "~s/version = $versionOld/version = $version/g" doc/xindex-doc.tex

echo "done!"
